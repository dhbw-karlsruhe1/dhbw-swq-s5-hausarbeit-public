# Hausarbeit SWQ S5 an der DHBW KA
- Hausarbeit des Faches "Softwarequalität" (SWQ) an der DHBW Karlsruhe im Studiengang (Angewandte) Informatik.
- Dieses Repo ist als Orientierung und Beispiel für Studis nach mir gedacht.
- Diese Abgabe erziehlte bei den Dozenten welche uns geprüft haben die Note 1.0 (100%).
- Context: Semester 5 - Jahrgang 20 (TINF20B5)

## **Preview**
[_bericht.pdf](https://dhbw-karlsruhe1.gitlab.io/dhbw-swq-s5-hausarbeit-public/_bericht.pdf)


## Build:
1. Installiere einen TeX Compiler. Empfohlen: [TeXLive](https://www.tug.org/texlive/)
2. Compile mit:

| Mode/OS | Auf Windows | Auf Linux |
| --- | --- | --- |
Direct start | Starte das [CompileTeX.bat](Compiler/CompileTeX.bat) script und lass es bis zum Ende durch laufen.| Starte das [CompileTeX.sh](Compiler/CompileTeX.sh) script und lass es bis zum Ende durch laufen. |
| periodical repeat mode |  Starte [Compile Scheduler.bat](Compile%20Scheduler.bat) | Not supported (yet) |

## Output:
Bei einem lokalen build wird eine `_bericht.pdf` im Root-folder des Repo's generiert.
