@echo off
Call :setup
goto looper
exit

:looper
    cls
    tasklist /FI "IMAGENAME eq Flo's Compiler*" 2>NUL | find /I /N "Flo's Compiler*" >NUL
    if "%ERRORLEVEL%"=="0" pause
    tasklist /FI "IMAGENAME eq CompileTeX*" 2>NUL | find /I /N "CompileTeX*" >NUL
    if "%ERRORLEVEL%"=="0" pause
    taskkill /FI "WINDOWTITLE eq Flo's Compiler*" /T /F
    taskkill /FI "WINDOWTITLE eq CompileTeX*" /T /F
    TIMEOUT /t 1
    echo I am in the path %mypath%
    start /min CompileTeX
    git update-index --assume-unchanged _bericht.pdf
    git update-index --assume-unchanged _Formalien.pdf
    timeout /t 180
goto looper

:setup
    set mypath=%cd%
    title LaTeX: Flo's Auto-Compiler-Scheduler ( in %mypath% )
    if not exist "build" mkdir build
    cls
    Echo Setup done.
    cd Compiler
goto :eof