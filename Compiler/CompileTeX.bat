Call :setup
cls
Call :copyToBuild
cd build
Call :compile
Call :movePDF
Call :cleanUp
cd ..
Exit

:setup
    set mypath=%cd%
    title Flo's Compiler ( in %mypath% )
    @Echo off
    cd ..
    if not exist "build" mkdir build
    cls
    Echo Setup done.
goto :eof

:copyToBuild
    Echo Cleaning build/...
    RD /S /Q build
    MD build
    Echo Copying to build/
    xcopy "TeX" "build" /s /e /y >>build/.move.log
goto :eof

:compile
    Echo Compiling...
    TIMEOUT /T 1 /nobreak >nul
    Echo|set /p="P"
    pdflatex _bericht
    
    TIMEOUT /T 1 /nobreak >nul
    Echo|set /p="B"
    bibtex _bericht
    
    TIMEOUT /T 1 /nobreak >nul
    Echo|set /p="P"
    pdflatex _bericht
    
    TIMEOUT /T 1 /nobreak >nul
    Echo|set /p="P"
    pdflatex _bericht
    
    TIMEOUT /T 1 /nobreak >nul
    Echo|set /p=" x"
    pdflatex _Formalien
    
    TIMEOUT /T 1 /nobreak >nul
    Echo -
    Echo Compilation done.
goto :eof

:movePDF
    
    TIMEOUT /T 1 /nobreak >nul
    move /y *.pdf ../
    Echo Moved pdf to ../
    powershell "[console]::beep(250,350)"
goto :eof

:cleanUp
    Echo Cleaning up...
    call :del_ext "*.aux" 
    call :del_ext "*.bbl"
    call :del_ext "*.blg"
    call :del_ext "*.fdb_latexmk"
    call :del_ext "*.fls"
    call :del_ext "*.for"
    call :del_ext "*.idx"
    call :del_ext "*.ilg"
    call :del_ext "*.ind"
    call :del_ext "*.lof"
    call :del_ext "*.lol"
    call :del_ext "*.lot"
    call :del_ext "*.synctex.gz"
    call :del_ext "*.toc"
    call :del_ext "*-blx.bib"
    call :del_ext "*.run.xml"
    
    call :del_ext "bericht.log"
    TIMEOUT /T 1 /nobreak >nul
    Echo Cleanup done.
goto :eof

:del_ext
 set del_ext=%1
 del /f /q /s "%del_ext%"
goto :eof